﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EersteGit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void imgRozen_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Red);
        }

        private void MouseLeave(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.LightGray);
        }

        private void imgGoudsbloem_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Orange);
        }

        private void imgLavendel_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Purple);
        }

        private void imgWittlerbitter_MouseEnter(object sender, MouseEventArgs e)
        {
            Background = new SolidColorBrush(Colors.Yellow);
        }
    }
}
